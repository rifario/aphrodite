import {
  Box,
  Button,
  List,
  ListItem,
  FormControl,
  FormLabel,
  FormErrorMessage,
  InputGroup,
  InputRightElement,
  Input,
  Icon,
  InputProps,
  Tag,
  TagLabel,
  TagCloseButton
} from '@chakra-ui/react'
import { useCombobox, useMultipleSelection } from 'downshift'
import { useController, useFormContext } from 'react-hook-form'

import { FiChevronDown } from 'react-icons/fi'
import { useState } from 'react'

type MultiselectProps = {
  name: string
  label: string
  options: Array<string>
  validation?: Record<string, unknown>
}

export default function Multiselect({
  options,
  label,
  name,
  validation,
  ...rest
}: MultiselectProps & InputProps): JSX.Element {
  const { control } = useFormContext()

  const {
    field: { ref, onChange, onBlur, ...inputProps },
    fieldState: { invalid, error }
  } = useController({
    name,
    control,
    rules: validation,
    defaultValue: ''
  })

  const [inputValue, setInputValue] = useState('')

  const {
    getSelectedItemProps,
    getDropdownProps,
    addSelectedItem,
    removeSelectedItem,
    selectedItems
  } = useMultipleSelection({
    onSelectedItemsChange: ({ selectedItems }) => onChange(selectedItems)
  })

  const getFilteredItems = () =>
    options.filter(
      item =>
        selectedItems.indexOf(item as never) < 0 &&
        item.toLowerCase().startsWith(inputValue.toLowerCase())
    )

  const {
    isOpen,
    getToggleButtonProps,
    getLabelProps,
    getMenuProps,
    getInputProps,
    getComboboxProps,
    highlightedIndex,
    getItemProps
  } = useCombobox({
    items: getFilteredItems(),
    defaultHighlightedIndex: 0,
    selectedItem: null,
    stateReducer: (_state, actionAndChanges) => {
      const { changes, type } = actionAndChanges
      switch (type) {
        case useCombobox.stateChangeTypes.InputKeyDownEnter:
        case useCombobox.stateChangeTypes.ItemClick:
          return {
            ...changes,
            isOpen: true // keep the menu open after selection.
          }
      }
      return changes
    },
    onStateChange: ({ inputValue, type, selectedItem }) => {
      switch (type) {
        case useCombobox.stateChangeTypes.InputChange:
          setInputValue(inputValue as string)
          break
        case useCombobox.stateChangeTypes.InputKeyDownEnter:
        case useCombobox.stateChangeTypes.ItemClick:
        case useCombobox.stateChangeTypes.InputBlur:
          if (selectedItem) {
            setInputValue('')
            addSelectedItem(selectedItem as never)
          }
          break
        default:
          break
      }
    }
  })

  return (
    <FormControl position="relative" isInvalid={invalid}>
      {label && (
        <FormLabel
          sx={{
            '&[data-invalid]': {
              color: 'red.500'
            }
          }}
          htmlFor={name}
          {...getLabelProps()}
        >
          {label}
        </FormLabel>
      )}
      <InputGroup {...getComboboxProps()} size="lg">
        <Input
          {...rest}
          {...getInputProps(getDropdownProps({ preventKeyAction: isOpen }))}
          variant="filled"
          focusBorderColor="primary.400"
          py="1.125rem"
          px="1.5rem"
          fontSize="md"
        />
        <InputRightElement h="full">
          <Button
            {...getToggleButtonProps({ onBlur })}
            borderRadius="lg"
            h="full"
            display="block"
            variant="ghost"
            colorScheme="blackAlpha"
            aria-label="toggle menu"
            _hover={{ bg: 'none' }}
          >
            <Icon as={FiChevronDown} />
          </Button>
        </InputRightElement>
      </InputGroup>
      <Box sx={{ '& span+span': { ml: 2 } }} mt={4}>
        {selectedItems.map((selectedItem, index) => (
          <Tag
            colorScheme="secondary"
            key={`selected-item-${index}`}
            {...getSelectedItemProps({ selectedItem, index })}
          >
            <TagLabel>{selectedItem as string}</TagLabel>
            <TagCloseButton
              onClick={e => {
                e.stopPropagation()
                removeSelectedItem(selectedItem)
              }}
            />
          </Tag>
        ))}
      </Box>
      <List
        overflowY="auto"
        maxH={64}
        w="full"
        zIndex="dropdown"
        position="absolute"
        top={20}
        left={0}
        borderBottomRadius="lg"
        as="ul"
        ref={ref}
        {...inputProps}
        {...getMenuProps()}
      >
        {isOpen &&
          getFilteredItems().map((option, index) => (
            <ListItem
              cursor="pointer"
              py="0.9rem"
              px="1.5rem"
              bg={highlightedIndex === index ? 'gray.200' : 'gray.100'}
              sx={{
                '&[data-selected]': {
                  bg:
                    highlightedIndex === index ? 'primary.200' : 'primary.300',
                  color: 'white'
                }
              }}
              {...getItemProps({ item: option, index })}
              key={`${option}${index}`}
            >
              {option}
            </ListItem>
          ))}
      </List>
      <FormErrorMessage>{error && error?.message}</FormErrorMessage>
    </FormControl>
  )
}
