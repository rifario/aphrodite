import {
  FormControl,
  FormErrorMessage,
  FormLabel,
  InputGroup,
  Textarea as ChakraTextarea,
  TextareaProps as ChakraTextareaProps,
  FormHelperText
} from '@chakra-ui/react'

import { get } from 'lodash'

import { useFormContext } from 'react-hook-form'

import { ReactNode } from 'react'

type TextareaProps = {
  name: string
  label?: string
  validation?: Record<string, unknown>
  formHelper?: ReactNode
}

export default function Textarea({
  name,
  label,
  validation,
  size = 'lg',
  formHelper,
  ...rest
}: TextareaProps & ChakraTextareaProps): JSX.Element {
  const {
    register,
    formState: { errors }
  } = useFormContext()

  return (
    <FormControl isInvalid={!!get(errors, name)}>
      {label && (
        <FormLabel
          sx={{
            '&[data-invalid]': {
              color: 'red.500'
            }
          }}
          htmlFor={name}
        >
          {label}
        </FormLabel>
      )}
      <InputGroup variant="filled" fontSize="md" size={size}>
        <ChakraTextarea
          focusBorderColor="primary.400"
          py="1.125rem"
          px="1.5rem"
          {...register(name, validation)}
          {...rest}
        />
      </InputGroup>
      <FormErrorMessage>
        {get(errors, name) && get(errors, `${name}.message`)}
      </FormErrorMessage>
      {formHelper && <FormHelperText>{formHelper}</FormHelperText>}
    </FormControl>
  )
}
