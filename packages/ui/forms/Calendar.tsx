import { Props, useDayzed } from 'dayzed'
import {
  Box,
  HStack,
  Heading,
  Icon,
  IconButton,
  Button,
  ButtonGroup,
  useBreakpointValue
} from '@chakra-ui/react'

import { HiChevronDoubleLeft, HiChevronDoubleRight } from 'react-icons/hi'
import { forwardRef, RefObject } from 'react'

const monthNamesShort = [
  'Jan',
  'Fev',
  'Mar',
  'Abr',
  'Mai',
  'Jun',
  'Jul',
  'Ago',
  'Set',
  'Out',
  'Nov',
  'Dez'
]
const weekdayNamesShort = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb']

function Calendar(
  props: Omit<Props, 'children' | 'render'>,
  ref: RefObject<HTMLDivElement>
): JSX.Element | null {
  const { calendars, getBackProps, getForwardProps, getDateProps } =
    useDayzed(props)

  if (!calendars.length) return null
  const dateSize = useBreakpointValue(['xs', 'sm'])

  return (
    <Box
      ref={ref}
      position="absolute"
      zIndex="dropdown"
      mt={2}
      right={0}
      maxW="30rem"
      mx="auto"
      textAlign="center"
      px={6}
      py={5}
      bg="gray.50"
      boxShadow="lg"
      borderRadius="lg"
      _focus={{
        boxShadow:
          '0 0 1px 2px rgba(88, 144, 255, .75), 0 1px 1px rgba(0, 0, 0, .15)'
      }}
    >
      <HStack justifyContent="space-between">
        <ButtonGroup size={dateSize} isAttached variant="outline">
          <IconButton
            mr="-2px"
            aria-label="Voltar um ano"
            _hover={{ bg: 'primary.50' }}
            _focus={{
              boxShadow:
                '0 0 1px 2px rgba(88, 144, 255, .75), 0 1px 1px rgba(0, 0, 0, .15)'
            }}
            icon={<Icon as={HiChevronDoubleLeft} />}
            {...getBackProps({ calendars, offset: 12 })}
          />
          <Button
            borderRadius="md"
            _hover={{ bg: 'primary.50' }}
            {...getBackProps({ calendars })}
          >
            Anterior
          </Button>
        </ButtonGroup>
        <ButtonGroup size={dateSize} isAttached variant="outline">
          <Button
            mr="-2px"
            borderRadius="md"
            _hover={{ bg: 'primary.50' }}
            {...getForwardProps({ calendars })}
          >
            Próximo
          </Button>
          <IconButton
            aria-label="Prosseguir um ano"
            _hover={{ bg: 'primary.50' }}
            icon={<Icon as={HiChevronDoubleRight} />}
            {...getForwardProps({ calendars, offset: 12 })}
          />
        </ButtonGroup>
      </HStack>
      {calendars.map(calendar => (
        <Box key={`${calendar.month}${calendar.year}`}>
          <Heading
            mt={5}
            size="sm"
            fontWeight="semibold"
            position="absolute"
            top={[1, 2]}
            left="50%"
            transform="translateX(-50%)"
          >
            {monthNamesShort[calendar.month]} {calendar.year}
          </Heading>
          {weekdayNamesShort.map(weekday => (
            <Box
              as="span"
              display="inline-block"
              fontWeight="bold"
              fontSize="xs"
              w="calc(100% / 7)"
              mt={6}
              mb={2}
              key={`${calendar.month}${calendar.year}${weekday}`}
            >
              {weekday}
            </Box>
          ))}
          {calendar.weeks.map((week, weekIndex) =>
            week.map((dateObj, index) => {
              const key = `${calendar.month}${calendar.year}${weekIndex}${index}`
              if (!dateObj) {
                return (
                  <Box
                    key={key}
                    as="span"
                    display="inline-block"
                    w="calc(100% / 7)"
                  />
                )
              }
              const {
                date,
                selected,
                selectable,
                today,
                prevMonth,
                nextMonth
              } = dateObj

              let background = today ? 'blue.50' : 'transparent'
              background = selected ? 'primary.300' : background
              background = !selectable ? 'secondary.100' : background

              let color = prevMonth || nextMonth ? 'gray.400' : 'black'
              color = selected ? 'white' : color

              return (
                <Button
                  size={dateSize}
                  fontWeight={selected ? 'bold' : 'normal'}
                  borderRadius="none"
                  key={key}
                  display="inline-block"
                  color={color}
                  _hover={{
                    bg: selected ? 'primary.200' : 'gray.200'
                  }}
                  bg={background}
                  w="calc(100% / 7)"
                  {...getDateProps({ dateObj })}
                >
                  {selectable ? date.getDate() : 'X'}
                </Button>
              )
            })
          )}
        </Box>
      ))}
    </Box>
  )
}

export default forwardRef(Calendar)
