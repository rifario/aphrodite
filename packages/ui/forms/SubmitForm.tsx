import { Button, ButtonProps } from '@chakra-ui/button'
import { ReactNode } from 'react'
import { useFormContext } from 'react-hook-form'

type SubmitFormProps = {
  children: ReactNode
}

export default function SubmitForm({
  children,
  ...rest
}: SubmitFormProps & ButtonProps): JSX.Element {
  const {
    formState: { isSubmitting }
  } = useFormContext()
  return (
    <Button {...rest} type="submit" isLoading={isSubmitting}>
      {children}
    </Button>
  )
}
