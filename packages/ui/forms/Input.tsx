import {
  FormControl,
  FormErrorMessage,
  FormLabel,
  InputGroup,
  Input as ChakraInput,
  InputProps as ChakraInputProps,
  InputLeftAddon,
  InputRightAddon,
  InputLeftElement,
  InputRightElement,
  FormHelperText
} from '@chakra-ui/react'

import { get } from 'lodash'

import MaskedInput from 'react-text-mask'
import createNumberMask from 'text-mask-addons/dist/createNumberMask'

import { useFormContext } from 'react-hook-form'

import { cloneElement, ReactElement, ReactNode } from 'react'

type InputProps = {
  name: string
  label?: string
  validation?: Record<string, unknown>
  rightElement?: ReactElement<typeof InputRightElement>
  rightAddon?: ReactElement<typeof InputRightAddon>
  leftElement?: ReactElement<typeof InputLeftElement>
  leftAddon?: ReactElement<typeof InputLeftAddon>
  formHelper?: ReactNode
  mask?: 'phone' | 'currency' | 'cpf'
}

export default function Input({
  name,
  label,
  validation,
  mask,
  size = 'lg',
  rightElement,
  leftElement,
  rightAddon,
  leftAddon,
  formHelper,
  ...rest
}: InputProps & ChakraInputProps): JSX.Element {
  const {
    register,
    formState: { errors }
  } = useFormContext()

  const MASK_MAP = {
    currency: createNumberMask({
      prefix: 'R$',
      suffix: '',
      includeThousandsSeparator: true,
      thousandsSeparatorSymbol: '.',
      allowDecimal: true,
      decimalSymbol: ',',
      decimalLimit: 2,
      allowNegative: false,
      allowLeadingZeroes: false
    }),
    cpf: [
      /^[0-9]$/,
      /^[0-9]$/,
      /^[0-9]$/,
      '.',
      /^[0-9]$/,
      /^[0-9]$/,
      /^[0-9]$/,
      '.',
      /^[0-9]$/,
      /^[0-9]$/,
      /^[0-9]$/,
      '-',
      /^[0-9]$/,
      /^[0-9]$/
    ],
    phone: [
      '(',
      /[1-9]/,
      /\d/,
      ')',
      ' ',
      /\d/,
      /\d/,
      /\d/,
      /\d/,
      /\d/,
      '-',
      /\d/,
      /\d/,
      /\d/,
      /\d/
    ]
  }

  return (
    <FormControl isInvalid={!!get(errors, name)}>
      {label && (
        <FormLabel
          sx={{
            '&[data-invalid]': {
              color: 'red.500'
            }
          }}
          htmlFor={name}
        >
          {label}
        </FormLabel>
      )}
      <InputGroup variant="filled" fontSize="md" size={size}>
        {leftAddon}
        {leftElement}
        {cloneElement(
          <ChakraInput
            focusBorderColor="primary.400"
            py="1.125rem"
            px="1.5rem"
            {...register(name, validation)}
            {...rest}
          />,
          mask && { as: MaskedInput, mask: MASK_MAP[mask] }
        )}
        {rightAddon}
        {rightElement}
      </InputGroup>
      <FormErrorMessage>
        {get(errors, name) && get(errors, `${name}.message`)}
      </FormErrorMessage>
      {formHelper && <FormHelperText>{formHelper}</FormHelperText>}
    </FormControl>
  )
}
