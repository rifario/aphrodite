import {
  Button,
  List,
  ListItem,
  FormControl,
  FormLabel,
  FormErrorMessage,
  InputGroup,
  InputRightElement,
  Input,
  Icon,
  InputProps
} from '@chakra-ui/react'
import { useCombobox } from 'downshift'
import { useController, useFormContext } from 'react-hook-form'

import { FiChevronDown } from 'react-icons/fi'
import { useState } from 'react'

type SelectProps = {
  name: string
  label: string
  acceptInputValue?: boolean
  options: Array<string>
  validation?: Record<string, unknown>
}

export default function Select({
  options,
  label,
  name,
  validation,
  acceptInputValue = false,
  ...rest
}: SelectProps & InputProps): JSX.Element {
  const { control } = useFormContext()

  const {
    field: { ref, onChange, onBlur, ...inputProps },
    fieldState: { invalid, error }
  } = useController({
    name,
    control,
    rules: validation,
    defaultValue: ''
  })

  const [inputItems, setInputItems] = useState(options)

  const {
    isOpen,
    getToggleButtonProps,
    getLabelProps,
    getMenuProps,
    getInputProps,
    getComboboxProps,
    highlightedIndex,
    getItemProps,
    selectedItem
  } = useCombobox({
    items: inputItems,
    onSelectedItemChange: ({ selectedItem }) => onChange(selectedItem),
    onInputValueChange: ({ inputValue }) => {
      setInputItems(
        options.filter(opt =>
          opt.toLowerCase().startsWith((inputValue as string).toLowerCase())
        )
      )

      if (acceptInputValue) {
        onChange(inputValue)
      }
    }
  })

  return (
    <FormControl position="relative" isInvalid={invalid}>
      {label && (
        <FormLabel
          sx={{
            '&[data-invalid]': {
              color: 'red.500'
            }
          }}
          htmlFor={name}
          {...getLabelProps()}
        >
          {label}
        </FormLabel>
      )}
      <InputGroup {...getComboboxProps()} size="lg">
        <Input
          {...rest}
          {...getInputProps()}
          variant="filled"
          focusBorderColor="primary.400"
          py="1.125rem"
          px="1.5rem"
          fontSize="md"
        />
        <InputRightElement h="full">
          <Button
            {...getToggleButtonProps({ onBlur })}
            borderRadius="lg"
            h="full"
            display="block"
            variant="ghost"
            colorScheme="blackAlpha"
            aria-label="toggle menu"
            _hover={{ bg: 'none' }}
          >
            <Icon as={FiChevronDown} />
          </Button>
        </InputRightElement>
      </InputGroup>
      <List
        overflowY="auto"
        maxH={64}
        w="full"
        zIndex="dropdown"
        position="absolute"
        top={20}
        left={0}
        borderBottomRadius="lg"
        as="ul"
        ref={ref}
        {...inputProps}
        {...getMenuProps()}
      >
        {isOpen &&
          inputItems.map((option, index) => (
            <ListItem
              data-selected={selectedItem === option || undefined}
              cursor="pointer"
              py="0.9rem"
              px="1.5rem"
              bg={highlightedIndex === index ? 'gray.200' : 'gray.100'}
              sx={{
                '&[data-selected]': {
                  bg:
                    highlightedIndex === index ? 'primary.200' : 'primary.300',
                  color: 'white'
                }
              }}
              {...getItemProps({ item: option, index })}
              key={`${option}${index}`}
            >
              {option}
            </ListItem>
          ))}
      </List>
      <FormErrorMessage>{error && error?.message}</FormErrorMessage>
    </FormControl>
  )
}
