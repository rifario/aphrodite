import {
  FormControl,
  FormLabel,
  FormErrorMessage,
  Input,
  InputGroup,
  InputRightElement,
  Icon,
  IconButton,
  useDisclosure,
  InputProps
} from '@chakra-ui/react'
import { DateObj } from 'dayzed'
import { KeyboardEvent, useRef } from 'react'
import { FiCalendar } from 'react-icons/fi'

import Calendar from './Calendar'
import { useController, useFormContext } from 'react-hook-form'

type DatepickerProps = {
  name: string
  label: string
  validation?: Record<string, unknown>
}

export default function Datepicker({
  name,
  validation,
  label,
  ...rest
}: DatepickerProps & InputProps): JSX.Element {
  const { isOpen, onToggle } = useDisclosure()

  const calendarRef = useRef<HTMLDivElement>(null)

  const { control } = useFormContext()

  const {
    field: { ref, onChange, value, name: inputName },
    fieldState: { invalid, error }
  } = useController({
    name,
    control,
    rules: validation,
    defaultValue: null
  })

  const handleSelect = ({ date }: DateObj) => {
    onChange(date.toLocaleDateString('pt-BR'))
    onToggle()
  }

  const handleKeyPress = (e: KeyboardEvent) => {
    e.preventDefault()
    if (e.code === 'Space') {
      onToggle()
      calendarRef.current?.focus()
    }
  }

  return (
    <FormControl isInvalid={invalid}>
      {label && (
        <FormLabel
          sx={{
            '&[data-invalid]': {
              color: 'red.500'
            }
          }}
          htmlFor={name}
        >
          {label}
        </FormLabel>
      )}
      <InputGroup variant="filled" fontSize="md" position="relative">
        <Input
          isReadOnly
          focusBorderColor="primary.400"
          py="1.125rem"
          px="1.5rem"
          ref={ref}
          onKeyDownCapture={handleKeyPress}
          size="lg"
          value={value as string}
          name={inputName}
          {...rest}
        />
        <InputRightElement h="full">
          <IconButton
            variant="ghost"
            aria-label="Open datepicker"
            colorScheme="gray"
            icon={<Icon as={FiCalendar} />}
            onClick={onToggle}
          />
        </InputRightElement>
      </InputGroup>
      <FormErrorMessage>{error && error?.message}</FormErrorMessage>
      {isOpen && (
        <Calendar
          ref={calendarRef}
          showOutsideDays
          selected={value as Date}
          onDateSelected={handleSelect}
        />
      )}
    </FormControl>
  )
}
