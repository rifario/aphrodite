/* eslint-disable @typescript-eslint/no-explicit-any */
import { chakra, ChakraProps } from '@chakra-ui/react'
import { ReactElement } from 'react'
import { FormProvider, useForm, UseFormReturn } from 'react-hook-form'

type FormProps = {
  defaultValues?: any
  children: ReactElement | ReactElement[]
  methods?: UseFormReturn<any>
  onSubmit: (data: Record<string, unknown>) => void
}

export default function Form({
  defaultValues = {},
  children,
  onSubmit,
  methods,
  ...rest
}: FormProps & ChakraProps): JSX.Element {
  const localMethods = useForm({ defaultValues })
  const formMethods = methods || localMethods

  return (
    <FormProvider {...formMethods}>
      <chakra.form {...rest} onSubmit={formMethods.handleSubmit(onSubmit)}>
        {children}
      </chakra.form>
    </FormProvider>
  )
}
