/* eslint-disable @typescript-eslint/no-explicit-any */

import {
  Badge,
  Flex,
  GridItem,
  Heading,
  Text,
  Button,
  Box,
  Skeleton
} from '@chakra-ui/react'
import { Sparkles } from '@rifario/components/atoms'

import { Link } from '../../atoms'

export interface Raffle {
  id: string
  title: string
  description: string
  price: string
  state: string
  drawDate: Date
  numberOfTickets: number
  numberOfTicketsByChart: number
  revenues: string
  numberOfTicketsSold: number
  numberOfTicketsPaid: number
  type: string
  userId: string
  createdAt: Date
  updatedAt: Date
  results: any[]
  medias: any[]
  prizes: any[]
  charts: any[]
  tickets: any[]
}

type RaffleCardProps = {
  raffle: Raffle
}

const formatNumberToWithDots = (number: number) =>
  number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')

const getFormattedDate = (date: Date) => {
  return date.toLocaleDateString('pt-BR', {
    year: 'numeric',
    month: 'short',
    day: 'numeric'
  })
}

export default function RaffleCard({ raffle }: RaffleCardProps): JSX.Element {
  const fresh = raffle.state === 'OPEN'
  return (
    <GridItem
      as="article"
      overflow="hidden"
      border="2px"
      borderColor="primary.400"
      borderRadius="2xl"
      bg="white"
    >
      <Flex
        as="header"
        direction="column"
        align="center"
        justify="center"
        position="relative"
        py={5}
        mb={8}
        bgGradient="linear(to-r, secondary.400 50%, primary.400 100%)"
        w="full"
        color="white"
      >
        <Heading maxW="20ch" isTruncated as="h1" size="md">
          {raffle.title}
        </Heading>
        <Text fontSize="sm">
          Sorteio em {getFormattedDate(new Date(raffle.drawDate))}
        </Text>
        {fresh && (
          <Box position="absolute" display="inline-block" bottom={-3}>
            <Sparkles>
              <Badge
                px={8}
                py={1}
                variant="solid"
                colorScheme="green"
                borderRadius="3xl"
              >
                Novo
              </Badge>
            </Sparkles>
          </Box>
        )}
      </Flex>
      <Box px={5} as="section">
        <Flex textAlign="center" justify="center" as="data">
          <Heading as="h2" fontWeight="bold">
            {formatNumberToWithDots(raffle.numberOfTickets)}
            <Box
              as="span"
              fontWeight="light"
              letterSpacing="normal"
              fontSize="sm"
              display="block"
            >
              Total de bilhetes
            </Box>
          </Heading>
        </Flex>
        <Box mt={12} as="footer">
          <Link href={`raffles/${raffle.id}`} replace>
            <Button variant="outline" w="full">
              Ver detalhes
            </Button>
          </Link>
        </Box>
      </Box>
    </GridItem>
  )
}

export const RaffleCardSkeleton: () => JSX.Element = () => (
  <GridItem
    as="article"
    overflow="hidden"
    border="2px"
    borderColor="gray.400"
    borderRadius="2xl"
    bg="white"
  >
    <Skeleton height="20px" width="50%" mx="auto" mt={5} />
    <Skeleton height="15px" width="70%" mx="auto" mt={1} />
    <Skeleton height="36px" width="70%" mx="auto" mt={12} />
    <Skeleton height="12px" width="70%" mx="auto" mt={1} />
  </GridItem>
)

RaffleCard.Skeleton = RaffleCardSkeleton
