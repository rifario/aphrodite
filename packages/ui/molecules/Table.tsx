/* eslint-disable react/display-name */
/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable react/jsx-key */
// @ts-nocheck

import {
  Table as ChakraTable,
  Thead,
  Flex,
  Tr,
  Th,
  Tbody,
  Td,
  Tag,
  TagLeftIcon,
  TagLabel,
  Box,
  Text,
  IconButton,
  ButtonGroup,
  Select,
  Stack,
  Icon,
  InputGroup,
  Input
} from '@chakra-ui/react'
import {
  ChangeEvent,
  createContext,
  ReactNode,
  useCallback,
  useContext,
  useMemo
} from 'react'

import { FaCircle } from 'react-icons/fa'
import {
  FiEdit,
  FiChevronsLeft,
  FiChevronsRight,
  FiChevronLeft,
  FiChevronRight,
  FiTrash
} from 'react-icons/fi'
import { HiSortAscending, HiSortDescending } from 'react-icons/hi'

import {
  usePagination,
  useSortBy,
  useTable,
  useGlobalFilter,
  Row
} from 'react-table'

type TStatus = 'PENDING' | 'PAID'

type StatusProps = {
  status: TStatus
}

type TableProps = {
  header: Array<{ label: string; id: string }>
  cells: Array<Record<string, unknown>>
  statusOnClick: (...args: any) => void
  clientOnClick: (...args: any) => void
  deleteOnClick: (...args: any) => void
  children: ReactNode
}

type TTableContext = {
  setPageSize: (pageSize: number) => void
  setGlobalFilter: (globalFilter: string) => void
  pageSize: number
}

function Status({ status }: StatusProps) {
  const COLOR_SCHEME_MAP = {
    PENDING: 'orange',
    PAID: 'green'
  }

  const STATUS_LABEL_MAP = {
    PENDING: 'Pendente',
    PAID: 'Pago'
  }

  return (
    <Tag
      colorScheme={COLOR_SCHEME_MAP[status]}
      fontSize="xs"
      px={2}
      py={1}
      borderRadius="3xl"
    >
      <TagLeftIcon boxSize="8px" as={FaCircle} />
      <TagLabel>{STATUS_LABEL_MAP[status]}</TagLabel>
    </Tag>
  )
}

const TableContext = createContext<TTableContext>({} as TTableContext)

export default function Table({
  header,
  cells,
  statusOnClick,
  clientOnClick,
  deleteOnClick,
  children
}: TableProps): JSX.Element {
  const columns = useMemo(
    () =>
      header.map(col => ({
        accessor: col.id,
        Header: col.label,
        Cell: (item: any) => {
          switch (col.id) {
            case 'status':
              return (
                <>
                  <Flex align="center">
                    <Status status={item.cell.value as TStatus} />
                    <IconButton
                      onClick={() => statusOnClick(item.row.index)}
                      aria-label="Mudar status da venda"
                      size="sm"
                      ml={2}
                      variant="ghost"
                      icon={<FiEdit />}
                    />
                    <IconButton
                      onClick={() => deleteOnClick(item.row.index)}
                      aria-label="Apagar venda"
                      colorScheme="red"
                      size="sm"
                      ml={2}
                      variant="ghost"
                      icon={<FiTrash />}
                    />
                  </Flex>
                </>
              )
            case 'name':
              return (
                <>
                  <Flex align="center">
                    <Box as="span">{item.cell.value}</Box>
                    <IconButton
                      onClick={() => clientOnClick(item.row.index)}
                      aria-label="Mudar titularidade dos bilhetes"
                      size="sm"
                      ml={2}
                      variant="ghost"
                      icon={<FiEdit />}
                    />
                  </Flex>
                </>
              )
            default:
              return <Box as="span">{item.cell.value}</Box>
          }
        }
      })),
    []
  )
  const data = useMemo(() => cells, [])

  const globalFilter = useCallback(
    (rows: Array<Row>, _columnIds: Array<string>, query: string) => {
      return rows.filter(
        row =>
          row.values.name.includes(query) ||
          row.values.address.includes(query) ||
          row.values.city.includes(query) ||
          row.values.tickets.includes(query)
      )
    },
    []
  )

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    setGlobalFilter,
    state: { pageIndex, pageSize }
  } = useTable(
    { columns, data, globalFilter },
    useGlobalFilter,
    useSortBy,
    usePagination
  ) as any

  return (
    <TableContext.Provider value={{ setPageSize, pageSize, setGlobalFilter }}>
      {children}
      <Box pb={14}>
        <ChakraTable {...getTableProps()} mt={6}>
          <Thead bg="primary.50">
            {headerGroups.map((headerGroup: any) => (
              <Tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column: any) => (
                  <Th {...column.getHeaderProps(column.getSortByToggleProps())}>
                    {column.render('Header')}
                    {column.isSorted ? (
                      <Icon ml={2} fontSize={16} as={HiSortDescending} />
                    ) : (
                      <Icon ml={2} fontSize={16} as={HiSortAscending} />
                    )}
                  </Th>
                ))}
              </Tr>
            ))}
          </Thead>
          <Tbody {...getTableBodyProps()}>
            {page.map((row: any) => {
              prepareRow(row)
              return (
                <Tr {...row.getRowProps()}>
                  {row.cells.map((cell: any) => {
                    return (
                      <Td {...cell.getCellProps()}>{cell.render('Cell')}</Td>
                    )
                  })}
                </Tr>
              )
            })}
          </Tbody>
        </ChakraTable>
        <Box
          position="absolute"
          left={0}
          right={0}
          bottom={0}
          textAlign="right"
          py={2}
          px={6}
          fontSize="sm"
          bg="primary.50"
          borderBottomRadius="2xl"
        >
          <Flex justify="space-between" align="center" as="footer">
            <ButtonGroup isAttached variant="outline">
              <IconButton
                onClick={() => gotoPage(0)}
                disabled={!canPreviousPage}
                aria-label="Ir para a primeira página"
                icon={<FiChevronsLeft />}
                mr="-2px"
              />
              <IconButton
                onClick={() => previousPage()}
                disabled={!canPreviousPage}
                aria-label="Página Anterior"
                icon={<FiChevronLeft />}
              />
            </ButtonGroup>

            <Text>
              Página {pageIndex + 1} de {pageOptions.length}
            </Text>

            <ButtonGroup isAttached variant="outline">
              <IconButton
                onClick={() => nextPage()}
                disabled={!canNextPage}
                aria-label="Próxima página"
                icon={<FiChevronRight />}
                mr="-2px"
              />
              <IconButton
                onClick={() => gotoPage(pageCount - 1)}
                disabled={!canNextPage}
                aria-label="Ir para a última página"
                icon={<FiChevronsRight />}
              />
            </ButtonGroup>
          </Flex>
        </Box>
      </Box>
    </TableContext.Provider>
  )
}

type THeaderProps = {
  filters?: {
    pageSize?: boolean
    name?: boolean
    address?: boolean
  }
}

function Header({
  filters = {
    pageSize: true
  }
}: THeaderProps) {
  const { setPageSize, pageSize, setGlobalFilter } = useContext(TableContext)

  const onPageSizeChange = (e: ChangeEvent<HTMLSelectElement>) => {
    const { value } = e.target
    setPageSize(Number(value))
  }

  return (
    <Stack p={6} direction="row" spacing={6} as="section">
      {filters && filters.pageSize && (
        <Select
          w="max-content"
          maxW="3xs"
          variant="filled"
          borderRadius="3xl"
          focusBorderColor="primary.400"
          size="lg"
          onChange={onPageSizeChange}
          value={pageSize}
        >
          {[10, 20, 30, 40, 50].map(pageSize => (
            <option key={pageSize} value={pageSize}>
              Mostar {pageSize}
            </option>
          ))}
        </Select>
      )}
      {filters && filters.name && (
        <InputGroup zIndex="base" maxW="md" size="lg">
          <Input
            onChange={e => setGlobalFilter(e.target.value)}
            variant="filled"
            borderRadius="3xl"
            focusBorderColor="primary.400"
            placeholder="Pesquisar por nome, endereço ou ticket"
          />
        </InputGroup>
      )}
    </Stack>
  )
}

Table.Header = Header
