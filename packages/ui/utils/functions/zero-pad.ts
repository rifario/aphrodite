export default function zeroPad(value: number, length: number): string {
  return `${value}`.padStart(length, '0')
}
