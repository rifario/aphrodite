import Router from 'next/router'
import toast from 'react-hot-toast'
import { assign, interpret, Machine } from 'xstate'

export type RaffleType = {
  name: string
  description: string
  prizeDrawDate: string

  tickets?: {
    amount: string
    value?: string
  }
  chart?: {
    ticketAmountByChart: string
    ticketAmount: string
    value?: string
  }

  prizes: { name: string; amount: number }[]
}

type RaffleContext = {
  raffle: Partial<RaffleType> | RaffleType | null
  formType: 'tickets' | 'chart'
  feedback: {
    raffle: boolean
    prizes: boolean
    confirm: boolean
  }
}

const initialContext: RaffleContext = {
  raffle: null,
  formType: 'tickets',
  feedback: {
    raffle: false,
    prizes: false,
    confirm: false
  }
}

const machine = Machine<RaffleContext>(
  {
    id: '@rifario/forms/new-raffle',
    initial: 'raffle',
    context: initialContext,
    states: {
      raffle: {
        on: {
          PREV: {
            actions: ['callback']
          },
          NEXT: {
            target: 'prizes',
            actions: ['updateContext']
          }
        }
      },
      prizes: {
        on: {
          PREV: 'raffle',
          NEXT: {
            target: 'confirm',
            actions: ['updateContext']
          }
        }
      },
      confirm: {
        on: {
          PREV: 'prizes',
          NEXT: { target: 'complete', actions: ['updateFeedback'] }
        }
      },
      complete: {
        entry: ['notifySuccess', 'redirectToRaffles'],
        type: 'final',
        exit: ['clearContext']
      }
    }
  },
  {
    actions: {
      updateContext: assign((ctx, e) => {
        const data = {
          ...ctx,
          raffle: { ...ctx.raffle, ...e.data },
          feedback: { ...ctx.feedback, ...e.feedback }
        }
        return data
      }),
      updateFeedback: assign((ctx, e) => {
        return {
          ...ctx,
          feedback: { ...ctx.feedback, ...e.feedback }
        }
      }),
      notifySuccess: () => {
        toast.success('Rifa criada com sucesso!', {
          duration: 3000,
          position: 'bottom-center',
          ariaProps: {
            role: 'status',
            'aria-live': 'polite'
          }
        })
      },
      redirectToRaffles: () => {
        Router.push('/raffles')
      },
      clearContext: assign(() => {
        return initialContext
      }),
      callback: (_ctx, e) => e.cb()
    }
  }
)

export default interpret(machine).start()
