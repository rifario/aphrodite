import { Box, Divider, Heading, Stack, Text, Flex } from '@chakra-ui/react'

import { useService } from '@xstate/react'
import MultiformRaffleService from '../../machines/multiform-raffle.machine'
import thoth from '../../services/api/thoth'
import toast from 'react-hot-toast'
import { useRouter } from 'next/router'
import { Form, SubmitForm } from '@rifario/components/forms'

export default function VerifyRaffleInfo(): JSX.Element {
  const router = useRouter()
  const [current, send] = useService(MultiformRaffleService)
  const { raffle } = current.context

  const handleConfirm = async () => {
    try {
      const [day, month, year] = raffle.prizeDrawDate.split('/')
      const drawDate = new Date(
        Number(year),
        Number(month) - 1,
        Number(day)
      ).toISOString()

      const ticketAmount = raffle.tickets?.amount || raffle.chart?.ticketAmount
      const numberOfTickets = Number(ticketAmount.replace('.', ''))

      const raffleRequest = {
        title: raffle.name,
        description: raffle.description,
        drawDate,
        price: Number(raffle.tickets?.value || raffle.chart.value),
        numberOfTickets,
        numberOfTicketsByChart: Number(
          raffle.chart?.ticketAmountByChart || raffle.tickets?.amount
        ),
        type: raffle.tickets?.value || raffle.chart.value ? 'PAID' : 'FREE',
        state: 'OPEN'
      }

      const response = await thoth.post('/raffle', raffleRequest)

      if (response.status === 201) {
        send({
          type: 'NEXT',
          feedback: { confirm: true }
        })
      } else if (response.status === 400) {
        toast.error(
          'Algo deu errado durante a criação da rifa. Verifique as informações e tente novamente',
          {
            duration: 3000,
            position: 'bottom-center',
            ariaProps: {
              role: 'status',
              'aria-live': 'assertive'
            }
          }
        )
      }
    } catch (error) {
      console.log(error)
      toast.error('Algo deu errado. Por favor tente novamente mais tarde', {
        duration: 3000,
        position: 'bottom-center',
        ariaProps: {
          role: 'status',
          'aria-live': 'assertive'
        }
      })
    }
  }

  return (
    <Form onSubmit={handleConfirm}>
      <Box>
        <Heading size="md" fontWeight="semibold" mb={4}>
          Resumo da rifa
        </Heading>

        <Box px={12} py={8} mb={36} borderRadius="xl" as="header" bg="gray.200">
          <Flex
            flexWrap="wrap"
            color="gray.500"
            sx={{
              gap: 12
            }}
          >
            <Text as="span">
              <strong>Nome da rifa: </strong>
              {raffle.name}
            </Text>
            <Text as="span">
              <strong>Data do sorteio: </strong>
              {raffle.prizeDrawDate}
            </Text>

            {raffle.tickets?.amount && (
              <Text as="span">
                <strong>Nº de bilhetes: </strong>
                {raffle.tickets.amount}
              </Text>
            )}
            {raffle.tickets?.value && (
              <Text as="span">
                <strong>Valor de cada bilhete: </strong>
                {raffle.tickets.value}
              </Text>
            )}
            {raffle.chart?.ticketAmount && (
              <Text as="span">
                <strong>Nº de bilhetes: </strong>
                {raffle.chart.ticketAmount}
              </Text>
            )}
            {raffle.chart?.ticketAmountByChart && (
              <Text as="span">
                <strong>Nº de bilhetes por cartela: </strong>
                {raffle.chart.ticketAmountByChart}
              </Text>
            )}
            {raffle.chart?.value && (
              <Text as="span">
                <strong>Valor de cada cartela: </strong>
                {raffle.chart.value}
              </Text>
            )}
            <Divider my={6} borderColor="gray.400" />
            {raffle.prizes.map(prize => (
              <Stack
                direction={{ base: 'column', sm: 'row' }}
                spacing={5}
                key={prize.name}
              >
                <Text>
                  <strong>Prêmio: </strong> {prize.name}
                </Text>
                <Text>
                  <strong>Quantidade: </strong> {prize.amount}
                </Text>
              </Stack>
            ))}
          </Flex>
        </Box>
        <Box mx="auto" as="footer" maxW="lg">
          <SubmitForm loadingText="Criando rifa" w="full">
            Finalizar
          </SubmitForm>
        </Box>
      </Box>
    </Form>
  )
}
