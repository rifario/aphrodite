import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Checkbox,
  Stack,
  Heading,
  Button
} from '@chakra-ui/react'
import { useForm } from 'react-hook-form'
import { Form, Number, SubmitForm } from '@rifario/components/forms'
import { useQueryClient } from 'react-query'
import { useRouter } from 'next/router'
import thoth from '../../../services/api/thoth'
import toast from 'react-hot-toast'

type MakePaymentModalProps = {
  onClose: () => void
  isOpen: boolean
  activeSale: {
    name: string
    address: string
    tickets: string
    value: number
    remainingValue: number
    status: string
    id: string
  } | null
}

export default function MakePaymentModal({
  isOpen,
  onClose,
  activeSale
}: MakePaymentModalProps): JSX.Element {
  const methods = useForm()
  const { watch, register } = methods
  const router = useRouter()
  const queryClient = useQueryClient()
  const { id } = router.query

  const isNotPaid = activeSale && activeSale.status !== 'PAID'

  const onSubmit = async (data: {
    payment?: number
    isFullPayment: boolean
  }) => {
    const payment = isFullPayment ? activeSale.remainingValue : data.payment

    try {
      if (isNotPaid && activeSale) {
        await thoth.put(`/sale/${id}/payment/${activeSale.id}`, {
          value: payment
        })
      } else {
        await thoth.put(`/sale/${id}/unpayment/${activeSale.id}`, {
          value: activeSale.value as number
        })
      }

      toast.success('Operação realizada com sucesso!', {
        duration: 3000,
        position: 'bottom-center',
        ariaProps: {
          role: 'status',
          'aria-live': 'polite'
        }
      })

      queryClient.invalidateQueries(['sales', { raffleId: id }])
      router.reload()
      onClose()
    } catch (error) {
      toast.error('Um erro desconhecido ocorreu. Tente novamente mais tarde', {
        duration: 3000,
        position: 'bottom-center',
        ariaProps: {
          role: 'status',
          'aria-live': 'assertive'
        }
      })
    }
  }

  const isFullPayment = watch('fullPayment', false)
  const returnPayment = watch('returnPayment', false)

  return (
    <Modal isOpen={isOpen} onClose={onClose} size="lg">
      <Form onSubmit={onSubmit} methods={methods}>
        <ModalOverlay />
        <ModalContent>
          {isNotPaid && <ModalHeader>Novo pagamento</ModalHeader>}
          <ModalBody mt={isNotPaid ? 2 : 12}>
            {isNotPaid ? (
              <>
                <Number
                  step={5}
                  min={0}
                  precision={2}
                  max={(activeSale?.value as number) || 0}
                  isDisabled={isFullPayment}
                  label="Valor do pagamento (R$)"
                  name="payment"
                  validation={{
                    required: isFullPayment
                      ? false
                      : 'O campo pagamento é obrigatório'
                  }}
                />
                <Checkbox size="md" mt={4} {...register('fullPayment')}>
                  Pagamento completo
                </Checkbox>
              </>
            ) : (
              <Stack spacing={12} align="center">
                <Heading textAlign="center">
                  Deseja devolver o pagamento?
                </Heading>
                <Checkbox size="md" mt={12} {...register('returnPayment')}>
                  Sim, fazer a devolução do pagamento
                </Checkbox>
              </Stack>
            )}
          </ModalBody>

          <ModalFooter mt={8}>
            <Button variant="ghost" mr={3} onClick={onClose}>
              Cancelar
            </Button>
            {isNotPaid ? (
              <SubmitForm>Concluir pagamento</SubmitForm>
            ) : (
              <SubmitForm isDisabled={!returnPayment}>
                Devolver pagamento
              </SubmitForm>
            )}
          </ModalFooter>
        </ModalContent>
      </Form>
    </Modal>
  )
}
