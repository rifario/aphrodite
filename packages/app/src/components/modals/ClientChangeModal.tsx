import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Stack,
  Button
} from '@chakra-ui/react'
import { useForm } from 'react-hook-form'
import { Form, Select, SubmitForm } from '@rifario/components/forms'
import { useQuery, useQueryClient } from 'react-query'
import { useRouter } from 'next/router'
import thoth from '../../../services/api/thoth'
import { ICity, IStreet } from '../../utils/types/thoth'
import { useEffect, useState } from 'react'
import toast from 'react-hot-toast'

type MakePaymentModalProps = {
  onClose: () => void
  isOpen: boolean
  activeSale: {
    name: string
    address: string
    tickets: string
    value: number
    remainingValue: number
    status: string
    id: string
  } | null
}

export default function ClientChangeModal({
  isOpen,
  onClose,
  activeSale
}: MakePaymentModalProps): JSX.Element {
  const methods = useForm()
  const { watch } = methods
  const router = useRouter()
  const queryClient = useQueryClient()
  const { id } = router.query

  const queryCities = async () => {
    const { data } = await thoth.get(`/cities`)
    return data
  }

  const queryClients = async () => {
    const { data } = await thoth.get(`/clients/${id}`)
    return data
  }

  const { data: cities, isLoading: isCitiesLoading } = useQuery<ICity[]>(
    'cities',
    queryCities
  )
  const { data: clients } = useQuery('clients', queryClients)

  const [cityNames, setCityNames] = useState<string[]>([])
  const [clientNames, setClientNames] = useState<string[]>([])
  const [streets, setStreets] = useState<IStreet[]>([])

  const streetNames = streets?.map(street => street.name) || []

  const filterCitiesByName = (name: string) => {
    const city = cities.find(
      city => city.name.toLowerCase() === name.toLowerCase()
    )
    return city
  }

  useEffect(() => {
    if (cities) {
      const res = cities.map(city => city.name)
      setCityNames(res)
    }

    if (clients) {
      const res = clients.map(client => client.name)
      setClientNames(res)
    }
  }, [cities, clients])

  useEffect(() => {
    const sub = watch(value => {
      if (!cities || isCitiesLoading || !value.city) return
      const city = filterCitiesByName(String(value.city))
      if (city) {
        const cityStreets = city.streets || []
        setStreets(cityStreets)
      }
    })

    return () => sub.unsubscribe()
  }, [watch])

  const onSubmit = async (data: {
    name: string
    city: string
    street: string
  }) => {
    if (!activeSale) return
    try {
      await thoth.put(`/sale/${id}/changeClient/${activeSale.id}`, {
        clientName: data.name,
        cityName: data.city,
        streetName: data.street
      })
      toast.success('Cliente alterado com sucesso!', {
        duration: 3000,
        position: 'bottom-center',
        ariaProps: {
          role: 'status',
          'aria-live': 'polite'
        }
      })
      queryClient.invalidateQueries(['sales', { raffleId: id }])
      router.reload()
      onClose()
    } catch (error) {
      toast.error('Um erro desconhecido ocorreu. Tente novamente mais tarde', {
        duration: 3000,
        position: 'bottom-center',
        ariaProps: {
          role: 'status',
          'aria-live': 'assertive'
        }
      })
    }
  }

  return (
    <Modal isOpen={isOpen} onClose={onClose} size="lg">
      <Form onSubmit={onSubmit} methods={methods}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Trocar titularidade</ModalHeader>
          <ModalBody mt={12}>
            <Stack spacing={8}>
              <Select
                acceptInputValue
                placeholder="João da Silva"
                name="name"
                label="Nome (ou apelido)"
                validation={{ required: 'O campo nome é obrigatório' }}
                options={clientNames}
              />
              <Select
                acceptInputValue
                validation={{ required: 'O campo cidade é obrigatório' }}
                placeholder="Cidade"
                name="city"
                label="Cidade"
                options={cityNames}
              />
              <Select
                acceptInputValue
                validation={{ required: 'O campo logradouro é obrigatório' }}
                placeholder="Rua Irmã Maria Regina"
                name="street"
                label="Logradouro"
                options={streetNames}
              />
            </Stack>
          </ModalBody>

          <ModalFooter mt={8}>
            <Button variant="ghost" mr={3} onClick={onClose}>
              Cancelar
            </Button>

            <SubmitForm>Confirmar mudança</SubmitForm>
          </ModalFooter>
        </ModalContent>
      </Form>
    </Modal>
  )
}
