import {
  Box,
  FormLabel,
  Grid,
  GridItem,
  Radio,
  RadioGroup,
  Stack
} from '@chakra-ui/react'

import { useForm } from 'react-hook-form'

import {
  Form,
  Input,
  Number,
  Select,
  Datepicker,
  SubmitForm,
  Textarea
} from '@rifario/components/forms'

import { useService } from '@xstate/react'
import { useEffect, useState } from 'react'

import MultiformRaffleService from '../../machines/multiform-raffle.machine'

export default function CreateRaffleData(): JSX.Element {
  const methods = useForm()
  const { register, control, handleSubmit, reset } = methods
  const [current, send] = useService(MultiformRaffleService)
  const [formType, setFormType] = useState<string>(current.context.formType)

  useEffect(() => {
    if (formType === 'tickets') {
      reset({ chart: null })
    } else {
      reset({ tickets: null })
    }
  }, [formType])

  const onSubmit = data => {
    send({ type: 'NEXT', data, feedback: { raffle: true }, formType })
  }

  return (
    <>
      <RadioGroup
        borderRadius="xl"
        name="form-type"
        w="full"
        mt={20}
        mb={8}
        onChange={setFormType}
        value={formType}
        defaultValue="tickets"
      >
        <FormLabel>Selecione o tipo de rifa que deseja: </FormLabel>
        <Stack bg="gray.100" py={5} justify="space-around" direction="row">
          <Radio borderColor="primary.400" value="tickets">
            Rifa de bilhetes
          </Radio>
          <Radio borderColor="primary.400" value="chart">
            Rifa de cartelas
          </Radio>
        </Stack>
      </RadioGroup>
      <Form
        methods={methods}
        defaultValues={current.context.raffle}
        onSubmit={onSubmit}
      >
        <Grid
          templateColumns={['1fr', '1fr', '1fr 1fr']}
          mb={[24, 24, 48]}
          gap={6}
        >
          <GridItem
            as="fieldset"
            display="flex"
            flexDirection="column"
            sx={{ gap: 24 }}
          >
            <Input
              validation={{ required: 'O campo nome é obrigatório' }}
              label="Nome"
              name="name"
              placeholder="Ex: Rifa de Natal"
            />
            <Textarea
              validation={{ required: 'O campo descrição é obrigatório' }}
              label="Descrição"
              name="description"
              placeholder="Descrição da rifa"
            />
            <Datepicker
              placeholder="Selecione uma data para o sorteio"
              validation={{
                required: 'O campo data do sorteio é obrigatório'
              }}
              label="Data do sorteio"
              name="prizeDrawDate"
            />
          </GridItem>
          {formType === 'chart' && (
            <GridItem
              as="fieldset"
              display="flex"
              flexDirection="column"
              sx={{ gap: 24 }}
            >
              <Select
                validation={{
                  required: 'O campo nº de bilhetes é obrigatório'
                }}
                name="chart.ticketAmount"
                label="Nº de bilhetes"
                options={[
                  '10',
                  '100',
                  '1.000',
                  '10.000',
                  '100.000',
                  '1.000.000'
                ]}
              />
              <Number
                min={1}
                validation={{
                  required: 'O campo nº de bilhetes por cartela é obrigatório'
                }}
                name="chart.ticketAmountByChart"
                label="Nº de bilhetes por cartela"
              />
              <Number
                min={1}
                label="Valor de cada cartela (em reais)"
                name="chart.value"
              />
            </GridItem>
          )}
          {formType === 'tickets' && (
            <GridItem
              as="fieldset"
              display="flex"
              flexDirection="column"
              sx={{ gap: 24 }}
            >
              <Select
                validation={{ required: 'O campo é obrigatório' }}
                name="tickets.amount"
                label="Nº de bilhetes"
                options={[
                  '10',
                  '100',
                  '1.000',
                  '10.000',
                  '100.000',
                  '1.000.000'
                ]}
              />
              <Number
                min={1}
                label="Valor de cada bilhete (em reais)"
                name="tickets.value"
              />
            </GridItem>
          )}
        </Grid>
        <Box mx="auto" as="footer" maxW="lg">
          <SubmitForm w="full">Próximo</SubmitForm>
        </Box>
      </Form>
    </>
  )
}
