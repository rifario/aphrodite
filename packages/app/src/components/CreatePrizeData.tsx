import { Box, Button, Divider, Grid, GridItem, Icon } from '@chakra-ui/react'

import { Form, Input, Number, SubmitForm } from '@rifario/components/forms'

import { FiPlus } from 'react-icons/fi'

import { useService } from '@xstate/react'
import { useForm, useFieldArray } from 'react-hook-form'

import MultiformRaffleService from '../../machines/multiform-raffle.machine'

const FiPlusIcon = <Icon as={FiPlus} />

type FormValues = {
  prizes: {
    name: string
    amount: number
  }[]
}

export default function CreatePrizeData(): JSX.Element {
  const [current, send] = useService(MultiformRaffleService)

  const createDefaultValues = () => {
    const defaultValues = current.context.raffle.prizes || [
      { name: '', amount: 1 }
    ]

    return defaultValues
  }

  const methods = useForm<FormValues>({
    defaultValues: {
      prizes: createDefaultValues()
    }
  })
  const { control } = methods
  const { fields, append, remove } = useFieldArray({ name: 'prizes', control })

  const onSubmit = data => {
    send({
      type: 'NEXT',
      data: {
        prizes: [...data.prizes]
      },
      feedback: {
        prizes: true
      }
    })
  }

  return (
    <Form methods={methods} onSubmit={onSubmit}>
      <Grid mb={[8, 8, 12]} gap={6}>
        {fields.map((field, index) => (
          <>
            <GridItem
              position="relative"
              key={field.id}
              as="fieldset"
              display="flex"
              flexDirection={['column', 'column', 'row']}
              sx={{ gap: 24 }}
            >
              <Input
                validation={{
                  required: 'O campo nome da recompensa é obrigatório'
                }}
                label="Nome da recompensa"
                name={`prizes.${index}.name` as const}
                placeholder="Ex: Mil reais"
                defaultValue={field.name}
              />
              <Number
                min={1}
                validation={{ required: 'O campo quantidade é obrigatório' }}
                label="Quantidade"
                name={`prizes.${index}.amount` as const}
                defaultValue={field.amount}
              />
              {fields.length > 1 && (
                <Button
                  onClick={() => remove(index)}
                  size="xs"
                  variant="ghost"
                  position="absolute"
                  right={0}
                >
                  Remover
                </Button>
              )}
            </GridItem>
            <Divider />
          </>
        ))}
      </Grid>
      <Button
        mb={40}
        display="flex"
        mx="auto"
        w="full"
        maxW="lg"
        variant="outline"
        rightIcon={FiPlusIcon}
        onClick={() => append({ name: '', amount: 1 })}
      >
        Adicionar novo prêmio
      </Button>
      <Box mx="auto" as="footer" maxW="lg">
        <SubmitForm w="full">Próximo</SubmitForm>
      </Box>
    </Form>
  )
}
