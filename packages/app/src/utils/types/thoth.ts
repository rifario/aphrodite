export interface ISale {
  id: string
  clientId: string
  value: string
  remainingValue: string
  status: string
  chartId: string
  createdAt: Date
  updatedAt: Date
  raffleId: string
}

export interface IChart {
  id: string
  raffleId: string
  value: string
  sale: ISale[]
}

export interface ITicket {
  id: string
  number: string
  won: boolean
  chartId: string
  raffleId: string
}

export interface IRaffle {
  id: string
  title: string
  description: string
  price: string
  state: string
  drawDate: Date
  numberOfTickets: number
  numberOfTicketsByChart: number
  revenues: string
  numberOfTicketsSold: number
  numberOfTicketsPaid: number
  type: string
  userId: string
  createdAt: Date
  updatedAt: Date
  results: any[]
  medias: any[]
  prizes: any[]
  charts: IChart[]
  tickets: ITicket[]
}

export interface IStreet {
  id: string
  name: string
  createdAt: Date
  updatedAt: Date
  cityId: string
}

export interface ICity {
  id: string
  name: string
  zipCode?: string
  state?: string
  country?: string
  createdAt: Date
  updatedAt: Date
  streets: IStreet[]
}
