import axios from 'axios'
import { GetServerSidePropsContext, GetServerSidePropsResult } from 'next'
import { parseCookies } from 'nookies'

const redirect = {
  redirect: {
    destination: '/login',
    permanent: false
  }
}

export default async function authenticate(
  ctx: GetServerSidePropsContext,
  props = {}
): Promise<GetServerSidePropsResult<{ [key: string]: never }>> {
  const refreshToken = parseCookies(ctx)['@charon/refresh']

  if (!refreshToken) {
    return redirect
  }

  return {
    props: props || {}
  }
}
