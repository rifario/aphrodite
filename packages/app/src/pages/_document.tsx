import Document, {
  DocumentContext,
  DocumentInitialProps,
  Html,
  Head,
  Main,
  NextScript
} from 'next/document'

class MyDocument extends Document {
  static async getInitialProps(
    ctx: DocumentContext
  ): Promise<DocumentInitialProps> {
    const initialProps = await Document.getInitialProps(ctx)
    return initialProps
  }

  render(): JSX.Element {
    return (
      <Html>
        <Head>
          <meta
            name="title"
            content="Rifar.io — Gerencie suas rifas de forma simples e intuitiva"
          />
          <meta
            name="description"
            content="O Rifar.io é uma plataforma para a criação e gestão de rifas de todos os tamanhos."
          />

          <meta property="og:type" content="website" />
          <meta property="og:url" content="https://app.rifar.io/" />
          <meta
            property="og:title"
            content="Rifar.io — Gerencie suas rifas de forma simples e intuitiva"
          />
          <meta
            property="og:description"
            content="O Rifar.io é uma plataforma para a criação e gestão de rifas de todos os tamanhos."
          />
          <meta
            property="og:image"
            content="https://res.cloudinary.com/app-rifar-io/image/upload/c_fill,h_628,w_1200/v1623895175/meta%20tags/meta-img.png"
          />

          <meta property="twitter:card" content="summary_large_image" />
          <meta property="twitter:url" content="https://app.rifar.io/" />
          <meta
            property="twitter:title"
            content="Rifar.io — Gerencie suas rifas de forma simples e intuitiva"
          />
          <meta
            property="twitter:description"
            content="O Rifar.io é uma plataforma para a criação e gestão de rifas de todos os tamanhos."
          />
          <meta
            property="twitter:image"
            content="https://res.cloudinary.com/app-rifar-io/image/upload/c_fill,h_628,w_1200/v1623895175/meta%20tags/meta-img.png"
          />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
