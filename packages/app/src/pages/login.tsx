import Image from 'next/image'
import { Box, Flex, Grid, GridItem, Heading, VStack } from '@chakra-ui/react'

import { Link } from '@rifario/components/atoms'
import { Form, Input, Password, SubmitForm } from '@rifario/components/forms'
import { useAuth } from '../../context/auth-context'
import { useEffect } from 'react'
import { useRouter } from 'next/router'

type LoginFormData = {
  username: string
  password: string
}

export default function Login(): JSX.Element {
  const router = useRouter()
  const { login, state } = useAuth()

  const handleLogin = async (data: LoginFormData) => {
    try {
      await login(data.username, data.password)
    } catch (error) {
      throw new Error(error.message)
    }
  }

  useEffect(() => {
    if (state.data) {
      router.push('/')
    }
  }, [state])

  return (
    <Grid
      minH="100vh"
      mx="auto"
      px={[6, 10]}
      placeItems="center"
      justifyContent="center"
      gap={20}
      templateColumns={[
        '1fr',
        '1fr',
        '1fr 1fr',
        '1fr 1fr',
        '0.6fr 0.6fr',
        '0.35fr 0.35fr'
      ]}
    >
      <GridItem as="aside">
        <Flex
          mx="auto"
          mb={[5, 7, 9]}
          as="span"
          align="center"
          aria-hidden="true"
          justify={['center', 'center', 'start']}
        >
          <Image width={135} height={135} src="/assets/hand-wave-emoji.svg" />
        </Flex>
        <Heading
          textAlign={['center', 'center', 'left']}
          maxW="5ch"
          fontSize={['7xl', '7xl', '8xl', '8xl', '9xl']}
          lineHeight="none"
        >
          Bem vindo!
        </Heading>
      </GridItem>
      <GridItem
        border="2px"
        borderColor="primary.400"
        borderRadius="xl"
        px={[8, 10, 10, 20]}
        py={[16, 16, 20]}
        as="main"
      >
        <Form onSubmit={handleLogin}>
          <VStack as="header" mb={16}>
            <Heading fontWeight="bold" size="md" as="h2">
              Fazer login
            </Heading>
            <Heading fontWeight="regular" size="sm" as="h3">
              Ir para área do cliente
            </Heading>
          </VStack>
          <VStack mb={24} spacing={5}>
            <Input
              validation={{ required: 'Campo usuário não pode ser vazio' }}
              placeholder="fulano@gmail.com"
              label="Usuário"
              name="username"
              id="username"
              autoComplete="email"
            />
            <Password
              validation={{ required: 'Campo senha não pode ser vazio' }}
              placeholder="Sua senha"
              label="Senha"
              name="password"
              id="password"
              autoComplete="current-password"
              formHelper={<Link href="#">Esqueceu sua senha?</Link>}
            />
          </VStack>
          <Box as="footer">
            <SubmitForm w="full" size="lg" loadingText="Fazendo login">
              Fazer login
            </SubmitForm>
          </Box>
        </Form>
      </GridItem>
    </Grid>
  )
}
