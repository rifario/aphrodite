import { Box, VStack, Stack, Heading, Icon, Text } from '@chakra-ui/react'
import { Form, Input, Select, SubmitForm } from '@rifario/components/forms'

import { FiArrowLeft } from 'react-icons/fi'
import { useRouter } from 'next/router'
import { GetServerSideProps } from 'next'
import authenticate from '../../../utils/functions/auth/authenticate'
import thoth from '../../../../services/api/thoth'
import toast from 'react-hot-toast'
import { useForm } from 'react-hook-form'
import { useEffect, useState } from 'react'
import { ICity, IStreet } from '../../../utils/types/thoth'
import { useQuery, useQueryClient } from 'react-query'

export default function Sale(): JSX.Element {
  const router = useRouter()
  const queryClient = useQueryClient()
  const raffleId = router.query.id as string
  const methods = useForm()

  const { watch, reset } = methods

  const queryCities = async () => {
    const { data } = await thoth.get(`/cities`)
    return data
  }

  const queryClients = async () => {
    const { data } = await thoth.get(`/clients/${raffleId}`)
    return data
  }

  const { data: cities, isLoading: isCitiesLoading } = useQuery<ICity[]>(
    'cities',
    queryCities
  )
  const { data: clients, isLoading: isClientsLoading } = useQuery(
    'clients',
    queryClients
  )

  const [cityNames, setCityNames] = useState<string[]>([])
  const [clientNames, setClientNames] = useState<string[]>([])
  const [streets, setStreets] = useState<IStreet[]>([])

  const streetNames = streets?.map(street => street.name) || []

  const filterCitiesByName = (name: string) => {
    const city = cities.find(
      city => city.name.toLowerCase() === name.toLowerCase()
    )
    return city
  }

  useEffect(() => {
    if (cities) {
      const res = cities.map(city => city.name)
      setCityNames(res)
    }

    if (clients) {
      const res = clients.map(client => client.name)
      setClientNames(res)
    }
  }, [cities, clients])

  useEffect(() => {
    const sub = watch(value => {
      if (!cities || isCitiesLoading || !value.city) return
      const city = filterCitiesByName(String(value.city))
      if (city) {
        const cityStreets = city.streets || []
        setStreets(cityStreets)
      }
    })

    return () => sub.unsubscribe()
  }, [watch])

  const onSubmit = async data => {
    // Replace empty spaces with nothing and remove commas
    const numbers = data.tickets.replace(' ', '').split(',')
    const tickets = numbers.map((number: string) => ({ number, won: false }))

    try {
      const raffleResponse = await thoth.get(`/raffle/${raffleId}`)
      const [raffle] = raffleResponse.data

      await thoth.post(`/sale/${raffleId}`, {
        cityName: data.city,
        clientName: data.name,
        streetName: data.street,
        status: 'PENDING',
        tickets,
        value: Number(raffle.price)
      })

      queryClient.invalidateQueries('streets')
      reset()

      toast.success('Venda realizada com sucesso', {
        duration: 3000,
        position: 'bottom-center',
        ariaProps: {
          role: 'status',
          'aria-live': 'polite'
        }
      })
    } catch (error) {
      console.error(error)

      toast.error('Um erro desconhecido ocorreu. Tente novamente mais tarde', {
        duration: 3000,
        position: 'bottom-center',
        ariaProps: {
          role: 'status',
          'aria-live': 'assertive'
        }
      })
    }
  }
  return (
    <Box mb={-24} bgGradient="linear(to-r, secondary.400, primary.400)">
      <Box
        h="full"
        as="section"
        bg="white"
        w={{ base: '90%', md: '60%' }}
        py={16}
        px={10}
        ml="auto"
        mr={{ base: 'auto', md: 0 }}
      >
        <Box
          as="button"
          onClick={router.back}
          letterSpacing="tighter"
          _hover={{ textDecoration: 'underline' }}
        >
          <Icon as={FiArrowLeft} />
          <Text as="span" verticalAlign="middle" ml={2}>
            Voltar
          </Text>
        </Box>
        <Form
          w={{ base: 'full', md: '80%' }}
          methods={methods}
          onSubmit={onSubmit}
        >
          <VStack as="fieldset" spacing={8} w="full" my={16}>
            <Heading as="legend" size="md" fontWeight="bold">
              Dados pessoais
            </Heading>
            <Select
              acceptInputValue
              placeholder="João da Silva"
              name="name"
              label="Nome (ou apelido)"
              validation={{ required: 'O campo nome é obrigatório' }}
              options={clientNames}
            />
          </VStack>
          <VStack as="fieldset" spacing={8} w="full" mb={16}>
            <Heading as="legend" size="md" fontWeight="bold">
              Dados residenciais
            </Heading>
            <Stack
              w="full"
              direction={{ base: 'column', sm: 'row' }}
              spacing={6}
            >
              <Select
                acceptInputValue
                validation={{ required: 'O campo cidade é obrigatório' }}
                placeholder="Cidade"
                name="city"
                label="Cidade"
                options={cityNames}
              />
              <Input
                placeholder="(99) 91234-5678"
                mask="phone"
                name="phone"
                label="Telefone"
              />
            </Stack>
            <Select
              acceptInputValue
              validation={{ required: 'O campo logradouro é obrigatório' }}
              placeholder="Rua Irmã Maria Regina"
              name="street"
              label="Logradouro"
              options={streetNames}
            />
          </VStack>
          <VStack as="fieldset" spacing={8} w="full">
            <Heading as="legend" size="md" fontWeight="bold">
              Selecionar bilhetes
            </Heading>
            <Input
              label="Bilhetes"
              name="tickets"
              formHelper="Separar os bilhetes por vírgula. (Ex: 0001, 0002, 0003, 0004)"
              validation={{
                required: 'Pelo menos um bilhete deve ser selecionado'
              }}
            />
          </VStack>

          <SubmitForm w="full" loadingText="Cadastrando venda" mt={40} mb={24}>
            Cadastrar venda
          </SubmitForm>
        </Form>
      </Box>
    </Box>
  )
}

export const getServerSideProps: GetServerSideProps = async ctx =>
  authenticate(ctx)
