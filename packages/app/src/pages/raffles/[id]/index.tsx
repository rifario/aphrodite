import { useRouter } from 'next/router'

import {
  Box,
  Container,
  Heading,
  Stack,
  Button,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  Icon,
  IconButton,
  useDisclosure
} from '@chakra-ui/react'
import { DataCard } from '@rifario/components/molecules/cards'

import { FiDollarSign, FiArrowLeft, FiShoppingCart } from 'react-icons/fi'
import { BiNote } from 'react-icons/bi'
import { RiListSettingsLine } from 'react-icons/ri'

import { Link } from '@rifario/components/atoms'
import { Table } from '@rifario/components/molecules'
import { GetServerSideProps, InferGetServerSidePropsType } from 'next'
import authenticate from '../../../utils/functions/auth/authenticate'
import { parseCookies } from 'nookies'
import axios from 'axios'

import { useState } from 'react'
import thoth from '../../../../services/api/thoth'
import { useQuery } from 'react-query'
import MakePaymentModal from '../../../components/modals/MakePaymentModal'
import ClientChangeModal from '../../../components/modals/ClientChangeModal'
import { useAuth } from '../../../../context/auth-context'

const COLUMNS = [
  { label: 'Nome completo', id: 'name' },
  { label: 'Cidade', id: 'city' },
  { label: 'Endereço', id: 'address' },
  { label: 'Tickets', id: 'tickets' },
  { label: 'Valor total', id: 'remainingValue' },
  { label: 'Status', id: 'status' }
]

export default function RaffleDetails({
  raffle
}: InferGetServerSidePropsType<typeof getServerSideProps>): JSX.Element {
  const { state } = useAuth()
  const router = useRouter()
  const { id } = router.query

  const { data: sales, isLoading } = useQuery(
    ['sales', { raffleId: id }],
    async () => {
      const { data: salesData } = await thoth.get(`/sale/${id}`)

      return salesData.map(sale => {
        return {
          name: sale.Client.name,
          city: sale.Client.Street.City.name,
          address: sale.Client.Street.name,
          tickets: sale.Chart.tickets.reduce((acc, ticket) => {
            return acc + ` ${ticket.number}`
          }, ''),
          value: parseInt(sale.value),
          remainingValue: parseInt(sale.remainingValue),
          status: sale.status,
          id: sale.id
        }
      })
    }
  )

  const [activeSale, setActiveSale] = useState(null)

  const {
    isOpen: isPaymentModalOpen,
    onOpen: onPaymentModalOpen,
    onClose: onPaymentModalClose
  } = useDisclosure()

  const {
    isOpen: isClientChangeModalOpen,
    onOpen: onClientChangeModalOpen,
    onClose: onClientChangeModalClose
  } = useDisclosure()

  const openClientChangeModal = (index: number) => {
    setActiveSale(sales[index])
    onClientChangeModalOpen()
  }

  const closeClientChangeModal = () => {
    setActiveSale(null)
    onClientChangeModalClose()
  }

  const openPaymentModal = (index: number) => {
    setActiveSale(sales[index])
    onPaymentModalOpen()
  }

  const closePaymentModal = () => {
    setActiveSale(null)
    onPaymentModalClose()
  }

  const handleDelete = async (index: number) => {
    await thoth.delete(`/sale/${id}/delete/${sales[index].id}`)
    router.reload()
  }

  return (
    <Container mb={16} as="main" maxW="container.xl">
      <Box
        pt={16}
        pb={48}
        mx="calc(-50vw + 50%)"
        bgGradient="linear(to-r, secondary.400, primary.400)"
        as="header"
      >
        <Container maxW="container.xl">
          <Link
            fontWeight="semibold"
            letterSpacing="tighter"
            color="white"
            href="/raffles"
          >
            <Icon mr={2} as={FiArrowLeft} display="inline-block" />
            <Box display="inline-block" as="span">
              Voltar
            </Box>
          </Link>

          <Heading size="2xl" mt={16} mx="auto" as="h1" color="white">
            {raffle.title}
          </Heading>
        </Container>
      </Box>
      <Stack
        as="header"
        mt={-20}
        mb={20}
        spacing={6}
        align="stretch"
        justify="space-between"
        direction={['column', 'column', 'row']}
      >
        {state.data && state.data.role === 'owner' && (
          <DataCard
            icon={FiDollarSign}
            label="Faturamento"
            value={`R$ ${
              raffle?.revenues?.toLocaleString('pt-BR', {
                currency: 'BRL',
                style: 'currency',
                minimumFractionDigits: 2
              }) || 0
            }`}
          />
        )}
        <DataCard
          icon={BiNote}
          label="Bilhetes vendidos"
          value={raffle.numberOfTicketsSold}
        />
        <DataCard
          icon={BiNote}
          label="Bilhetes pagos"
          value={raffle.numberOfTicketsPaid}
        />
      </Stack>

      <Stack mb={16} as="section" direction="row" justify="space-between">
        <Heading as="h2">Seus clientes</Heading>

        <Menu isLazy>
          <MenuButton
            borderRadius="xl"
            variant="outline"
            display={{ base: 'inline-flex', md: 'none' }}
            as={IconButton}
            aria-label="Options"
            icon={<Icon as={RiListSettingsLine} />}
          >
            Abrir menu
          </MenuButton>
          <MenuList>
            <MenuItem
              as={Link}
              transition="0.3s"
              _hover={{ textDecoration: 'none', bg: 'primary.50' }}
              href={`${id}/sale`}
              icon={<Icon as={FiShoppingCart} />}
            >
              Realizar venda
            </MenuItem>
          </MenuList>
        </Menu>

        <Stack
          display={{ base: 'none', md: 'flex' }}
          direction="row"
          spacing={6}
        >
          <Link href={`${id}/sale`}>
            <Button leftIcon={<Icon as={FiShoppingCart} />}>
              Realizar venda
            </Button>
          </Link>
        </Stack>
      </Stack>
      <Box
        borderRadius="2xl"
        border="1px"
        borderColor="primary.400"
        position="relative"
      >
        {!isLoading && sales && (
          <Box as="section" overflowX="auto">
            <Table
              deleteOnClick={handleDelete}
              clientOnClick={openClientChangeModal}
              statusOnClick={openPaymentModal}
              header={COLUMNS}
              cells={sales}
            >
              <Table.Header filters={{ pageSize: true, name: true }} />
              <MakePaymentModal
                activeSale={activeSale}
                isOpen={isPaymentModalOpen}
                onClose={closePaymentModal}
              />
              <ClientChangeModal
                activeSale={activeSale}
                isOpen={isClientChangeModalOpen}
                onClose={closeClientChangeModal}
              />
            </Table>
          </Box>
        )}
      </Box>
    </Container>
  )
}

export const getServerSideProps: GetServerSideProps = async ctx => {
  try {
    const id = ctx.params.id

    const accessToken = parseCookies(ctx)['@charon/access']

    const raffleResponse = await axios.get(
      `${process.env.NEXT_PUBLIC_THOTH_API}/raffle/${id}`,
      {
        headers: {
          Authorization: `Bearer ${accessToken}`
        }
      }
    )

    const [raffle] = raffleResponse.data

    return authenticate(ctx, {
      raffle
    })
  } catch (err) {
    return authenticate(ctx)
  }
}
