import {
  Box,
  Heading,
  Container,
  Grid,
  Flex,
  Button,
  Icon,
  Text
} from '@chakra-ui/react'

import { RaffleCard } from '@rifario/components/molecules/cards'
import { GetServerSideProps } from 'next'

import thoth from '../../../services/api/thoth'
import authenticate from '../../utils/functions/auth/authenticate'

import { useRouter } from 'next/router'
import { FiArrowRight, FiArrowLeft } from 'react-icons/fi'
import { useQuery } from 'react-query'
import { IRaffle } from '../../utils/types/thoth'

export default function Raffles(): JSX.Element {
  const router = useRouter()
  const { isLoading, data: raffles } = useQuery<IRaffle[]>(
    'raffles',
    async () => {
      const { data } = await thoth.get('/raffle')
      return data
    }
  )

  const isRafflesEmpty = !raffles || raffles.length === 0

  return (
    <Container pt={20} maxW="container.lg" as="main">
      <Box mb={20} as="header">
        <Button
          onClick={() => router.push('/')}
          letterSpacing="tighter"
          variant="ghost"
          mb={12}
        >
          <Icon as={FiArrowLeft} />
          <Text as="span" verticalAlign="middle" ml={2}>
            Voltar ao dashboard
          </Text>
        </Button>
        <Heading as="h1" size="xl">
          Minhas rifas
        </Heading>
      </Box>
      <Grid
        as="section"
        gap={4}
        templateColumns="repeat(auto-fill, minmax(15rem, 1fr))"
        autoRows="minmax(18rem, 1fr)"
      >
        {isLoading ? (
          Array(6)
            .fill(0)
            .map((_, index) => <RaffleCard.Skeleton key={index} />)
        ) : isRafflesEmpty ? (
          <Flex
            flexDir="column"
            gridColumn="1/-1"
            justifyContent="center"
            alignItems="center"
            textAlign="center"
            border="2px solid"
            borderColor="gray.400"
            borderRadius="md"
            color="gray.500"
          >
            Você ainda não possui nenhuma rifa. Que tal criar uma?
            <Button
              variant="outline"
              onClick={() => router.push('/raffles/new')}
              mt={6}
              rightIcon={<Icon as={FiArrowRight} />}
            >
              Criar nova rifa
            </Button>
          </Flex>
        ) : (
          raffles.map(raffle => <RaffleCard key={raffle.id} raffle={raffle} />)
        )}
      </Grid>
    </Container>
  )
}

export const getServerSideProps: GetServerSideProps = async ctx =>
  authenticate(ctx)
