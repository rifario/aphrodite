import Head from 'next/head'
import { AppProps } from 'next/app'

import Provider from '@rifario/components/provider'
import { Footer, Navbar } from '@rifario/components/molecules'
import { ReactQueryDevtools } from 'react-query/devtools'

import {
  Box,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Icon
} from '@chakra-ui/react'

import AuthProvider, { useAuth } from '../../context/auth-context'
import { ReactNode } from 'react'

import { FiChevronDown, FiLogOut } from 'react-icons/fi'
import { Toaster } from 'react-hot-toast'
import { Hydrate, QueryClient, QueryClientProvider } from 'react-query'

function Body({ children }: { children: ReactNode }): JSX.Element {
  const {
    state: { data: user },
    logout
  } = useAuth()
  const queryClient = new QueryClient()
  return (
    <QueryClientProvider client={queryClient}>
      <ReactQueryDevtools />
      <Box
        pt={16}
        pb={36}
        position="relative"
        minH="100vh"
        overflowX="hidden"
        as="main"
      >
        <Navbar bg="black" color="white" breakpoint="sm">
          {user && (
            <Menu isLazy>
              <MenuButton>
                Olá, <strong>{user.name}</strong>
                <Icon ml={2} as={FiChevronDown} />
              </MenuButton>
              <MenuList color="black">
                <MenuItem
                  onClick={() => logout()}
                  icon={<Icon as={FiLogOut} />}
                >
                  Fazer logout
                </MenuItem>
              </MenuList>
            </Menu>
          )}
        </Navbar>
        {children}
        <Footer>
          © Copyright 2021 - Rifar.io - Todos os direitos reservados
        </Footer>
        <Toaster />
      </Box>
    </QueryClientProvider>
  )
}

export default function App({ Component, pageProps }: AppProps): JSX.Element {
  return (
    <Provider>
      <AuthProvider>
        <Head>
          <title>Rifar.io | Área do usuário</title>
        </Head>
        <Body>
          <Hydrate state={pageProps.dehydratedState}>
            <Component {...pageProps} />
          </Hydrate>
        </Body>
      </AuthProvider>
    </Provider>
  )
}
