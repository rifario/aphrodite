import Router from 'next/router'

import {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState
} from 'react'

import toast from 'react-hot-toast'
import charon from '../../services/api/charon'

import { clearToken, setToken } from '../../services/token'

type User = {
  name: string
  email: string
  id: string
  role: 'owner' | 'employer'
}

type State = {
  status: 'idle' | 'loading' | 'error' | 'success'
  error: string | null
  data: User | null
}

type Context = {
  state: State
  login: (email: string, password: string) => Promise<void>
  logout: () => Promise<void>
}

type AuthProviderProps = {
  children: ReactNode
}

const AuthContext = createContext({} as Context)

export default function AuthProvider({
  children
}: AuthProviderProps): JSX.Element {
  const [state, setState] = useState<State>({
    status: 'idle',
    error: null,
    data: null
  })

  useEffect(() => {
    ;(async () => {
      setState(old => ({
        ...old,
        status: 'loading',
        error: null
      }))

      if (!state.data) {
        try {
          const { data } = await charon.get('/reload')

          setState(old => ({ ...old, data, status: 'success' }))
        } catch (error) {
          setState(old => ({ ...old, error: error.message, status: 'error' }))
          Router.push('/login')
        }
      } else {
        setState(old => ({ ...old, status: 'success', error: null }))
      }
    })()
  }, [])

  async function login(email: string, password: string) {
    setState({
      status: 'loading',
      error: null,
      data: null
    })

    try {
      const response = await charon.post('/login', {
        email,
        password
      })

      if (response.status === 200) {
        const { data } = response

        setState({
          status: 'success',
          error: null,
          data: data.user
        })

        setToken(data.accessToken, data.refreshToken)

        toast.success('Login bem sucedido!', {
          duration: 3000,
          position: 'bottom-center',
          ariaProps: {
            role: 'status',
            'aria-live': 'polite'
          }
        })

        Router.push('/')
      } else if (response.status === 400) {
        const error =
          'Senha ou e-mail inválida. Verifique suas credenciais e tente novamente.'
        setState({
          status: 'error',
          error,
          data: null
        })

        toast.error(error, {
          duration: 3000,
          position: 'bottom-center',
          ariaProps: {
            role: 'status',
            'aria-live': 'assertive'
          }
        })
      }
    } catch (error) {
      setState({
        status: 'error',
        error: error.message,
        data: null
      })

      toast.error(error.message, {
        duration: 3000,
        position: 'bottom-center',
        ariaProps: {
          role: 'status',
          'aria-live': 'assertive'
        }
      })
    }
  }

  async function logout(): Promise<void> {
    setState(old => ({ ...old, status: 'loading' }))

    try {
      await charon.get(`/logout/${state.data.id}`)

      clearToken()

      setState({
        status: 'success',
        error: null,
        data: null
      })

      toast.success('Logout feito com sucesso!', {
        duration: 3000,
        position: 'bottom-center',
        ariaProps: {
          role: 'status',
          'aria-live': 'polite'
        }
      })
      Router.push('/login')
    } catch (error) {
      setState(old => ({ ...old, status: 'error', error: error.message }))
      toast.error(error.message, {
        duration: 3000,
        position: 'bottom-center',
        ariaProps: {
          role: 'status',
          'aria-live': 'assertive'
        }
      })
    }
  }

  return (
    <AuthContext.Provider value={{ state, login, logout }}>
      {children}
    </AuthContext.Provider>
  )
}

export const useAuth = (): Context => useContext(AuthContext)
