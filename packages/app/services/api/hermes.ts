import axios from 'axios'
import { getToken, setToken } from '../token'

const hermes = axios.create({
  baseURL: process.env.NEXT_PUBLIC_HERMES_API
})

hermes.interceptors.request.use(
  config => {
    const { accessToken } = getToken()
    if (accessToken) {
      config.headers.Authorization = `Bearer ${accessToken}`
    }
    config.headers['Content-Type'] = 'application/json'
    return config
  },
  error => {
    Promise.reject(error)
  }
)

hermes.interceptors.response.use(
  response => {
    return response
  },
  async error => {
    const originalRequest = error.config
    if (
      error.response.status === 401 &&
      originalRequest.url === `${process.env.NEXT_PUBLIC_CHARON_API}/token`
    ) {
      return Promise.reject(error)
    }

    if (error.response.status === 401 && !originalRequest._retry) {
      originalRequest._retry = true
      const { refreshToken } = getToken()
      const response = await axios.post(
        `${process.env.NEXT_PUBLIC_CHARON_API}/token`,
        { refreshToken }
      )

      if (response.status === 200) {
        const { accessToken } = response.data
        setToken(accessToken, refreshToken)

        hermes.defaults.headers.Authorization = `Bearer ${accessToken}`
        originalRequest.headers.Authorization = `Bearer ${accessToken}`

        return hermes(originalRequest)
      }

      return Promise.reject(error.response.data)
    }
    return Promise.reject(error)
  }
)

export default hermes
