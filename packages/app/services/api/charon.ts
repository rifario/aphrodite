import axios from 'axios'
import { getToken, setToken } from '../token'

const charon = axios.create({
  baseURL: process.env.NEXT_PUBLIC_CHARON_API
})

charon.interceptors.request.use(
  config => {
    const { accessToken } = getToken()

    if (accessToken) {
      config.headers.Authorization = `Bearer ${accessToken}`
    }
    config.headers['Content-Type'] = 'application/json'
    return config
  },
  error => {
    Promise.reject(error)
  }
)

charon.interceptors.response.use(
  response => {
    return response
  },
  async error => {
    console.log(error)
    const originalRequest = error.config
    if (
      error.response.status === 401 &&
      originalRequest.url === `${process.env.NEXT_PUBLIC_CHARON_API}/token`
    ) {
      return Promise.reject(error)
    }

    if (error.response.status === 401 && !originalRequest._retry) {
      originalRequest._retry = true
      const { refreshToken } = getToken()
      const response = await axios.post(
        `${process.env.NEXT_PUBLIC_CHARON_API}/token`,
        { refreshToken }
      )

      if (response.status === 200) {
        const { accessToken } = response.data
        setToken(accessToken, refreshToken)

        charon.defaults.headers.Authorization = `Bearer ${accessToken}`
        originalRequest.headers.Authorization = `Bearer ${accessToken}`

        return charon(originalRequest)
      }

      return Promise.reject(error.response.data)
    }
    return Promise.reject(error)
  }
)

export default charon
