import { destroyCookie, parseCookies, setCookie } from 'nookies'

function getToken(): { accessToken: string; refreshToken: string } {
  const accessToken = parseCookies(null)['@charon/access']
  const refreshToken = parseCookies(null)['@charon/refresh']

  return { accessToken, refreshToken }
}

function setToken(accessToken: string, refreshToken: string): void {
  setCookie(null, '@charon/access', accessToken, {
    maxAge: 60 * 10, // 10 minutes
    sameSite: 'strict',
    secure: process.env.NODE_ENV === 'production',
    path: '/'
  })
  setCookie(null, '@charon/refresh', refreshToken, {
    maxAge: 60 * 60 * 24, // 1 day
    sameSite: 'strict',
    secure: process.env.NODE_ENV === 'production',
    path: '/'
  })
}

function clearToken(): void {
  destroyCookie(null, '@charon/refresh', {
    path: '/'
  })
  destroyCookie(null, '@charon/access', {
    path: '/'
  })
}

export { getToken, setToken, clearToken }
